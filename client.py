import requests
from typing import Dict, Any


def get_username() -> str:
    return input("Who are you?\t")


def get_room_id() -> str:
    return input("Please, enter room id:\t")


def get_messages(room_id: str, url: str) -> Dict[str, Any]:
    return requests.get(f"{url}/{room_id}").json()


def leave_room():
    answer = input("Are you sure you want to live the chat?(yes/no)")
    if answer.upper() == 'YES':
        return 1
    else:
        return 0


def print_messages(messages):
    for i in messages["messages"]:
        print(i)


def send_message(message: str, username: str, room_id: str, url: str) -> Dict[str, Any]:
    return requests.post(f"{url}/{room_id}", json={"message": message, "username": username}).json()


def main(url):
    username = get_username()
    room_id = get_room_id()
    messages = get_messages(room_id=room_id, url=url)
    print("The chat has started")
    print_messages(messages)
    while True:
        your_message = input(">")
        if your_message.upper() == "/LEAVE":
            if leave_room() == 1:
                main(url=url)
        response = send_message(message=your_message, username=username, room_id=room_id, url=url)
        print_messages(response)


if __name__ == '__main__':
    main(url="http://localhost:5001")

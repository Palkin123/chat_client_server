from flask import *

app = Flask(__name__)

database = {}
if open("database.txt").read() == '':
    database = {}
else:
    js = open("database.txt").read()
    database = json.loads(js)


@app.route('/')
def homepage():
    return 'homepage'


@app.route('/<string:room_id>', methods=['GET'])
def start_chat(room_id: str):
    if not database.get(room_id, None):
        database[room_id] = []
        return jsonify(messages=database[room_id])
    return jsonify(messages=database[room_id])


@app.route('/<string:room_id>', methods=['POST'])
def apply_message(room_id: str):
    req = request.get_json()
    if req["message"] == '':
        return jsonify(messages=database[room_id])
    message = req["message"]
    username = req["username"]
    database[room_id].append(f"{username}:\t{message}")
    json.dump(database, open("database.txt", "w"))
    return jsonify(messages=database[room_id])


if __name__ == "__main__":
    app.run(host="localhost", port=5001)
